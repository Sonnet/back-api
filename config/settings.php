<?php
//This file contains the debug and dev settings of the api
 ini_set('display_errors','1'); // set to 0 in production

 //Put all the files requires by the api below
 require './db/connection.php';

 //Put all the components here
 require './components/v1/about.php';
 require './components/v1/authLogin.php';
 ?>