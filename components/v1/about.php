<?php
//This file contains the about route of the api
function about($request, $response) {
    $json = $request->getBody();
    $data = json_decode($json, true);
    $rs = array(
        "name" =>"Back-Rest API",
        "version" => "0.0.1",
        "author" => "Pavitra Behre",
        "release" => "5th January, 2017",
    );
    $rs = json_encode($rs);
    return $rs;
}
?>